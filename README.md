# Colormaps using CIELab space

The `wxmpl-spline.py` script shows (interactively) a colormap based on points
in the lightness, green-red and blue-yellow spaces. It is corrected locally
along the entire spectrum using the `color.deltaE_ciede94` color correction
calculation from `scikit-image`. It does not ensure that the colormap is linear
in luminosity but it does try to make local differences visible.

![Colormap Interactive Editor](colormap_generator.png)
